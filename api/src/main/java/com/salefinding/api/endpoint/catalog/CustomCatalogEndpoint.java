package com.salefinding.api.endpoint.catalog;

import com.broadleafcommerce.rest.api.endpoint.catalog.CatalogEndpoint;
import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.salefinding.api.endpoint.wrapper.CustomCategoryWrapper;
import com.salefinding.api.endpoint.wrapper.CustomerProductWrapper;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/catalog/", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class CustomCatalogEndpoint extends CatalogEndpoint {

    @RequestMapping(value = "product/add", method = RequestMethod.POST)
    public ProductWrapper addProduct(HttpServletRequest request, @RequestBody CustomerProductWrapper wrapper) {
        Product product = wrapper.unwrap(request, context);
        catalogService.saveProduct(product);

        ProductWrapper newProductWrapper = (ProductWrapper)this.context.getBean(ProductWrapper.class.getName());
        newProductWrapper.wrapDetails(product, request);

        return newProductWrapper;
    }

    @RequestMapping(value = "category/add", method = RequestMethod.POST)
    public CategoryWrapper addCategory(HttpServletRequest request, @RequestBody CustomCategoryWrapper wrapper) {
        Category category = wrapper.unwrap(request, context);
        catalogService.saveCategory(category);

        CategoryWrapper newCategoryWrapper = (CategoryWrapper)this.context.getBean(CategoryWrapper.class.getName());
        newCategoryWrapper.wrapDetails(category, request);

        return newCategoryWrapper;
    }
}
