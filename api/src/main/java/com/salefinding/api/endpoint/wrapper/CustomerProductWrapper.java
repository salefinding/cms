package com.salefinding.api.endpoint.wrapper;

import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.salefinding.api.util.UnwrapHelper;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;

public class CustomerProductWrapper extends ProductWrapper implements APIUnwrapper<Product> {

    @Override
    public Product unwrap(HttpServletRequest httpServletRequest, ApplicationContext applicationContext) {
        return UnwrapHelper.unwrapProduct(this, applicationContext);
    }
}
