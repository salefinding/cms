package com.salefinding.api.endpoint.wrapper;

import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.salefinding.api.util.UnwrapHelper;
import org.broadleafcommerce.common.rest.api.wrapper.APIUnwrapper;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;

public class CustomCategoryWrapper extends CategoryWrapper implements APIUnwrapper<Category> {

    @Override
    public Category unwrap(HttpServletRequest httpServletRequest, ApplicationContext applicationContext) {
        return UnwrapHelper.unwrapCategory(this, applicationContext);
    }
}
