package com.salefinding.api.util;

import com.broadleafcommerce.rest.api.wrapper.*;
import org.broadleafcommerce.common.media.domain.Media;
import org.broadleafcommerce.common.media.domain.MediaImpl;
import org.broadleafcommerce.core.catalog.domain.*;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.broadleafcommerce.core.catalog.service.type.ProductType;
import org.broadleafcommerce.core.inventory.service.type.InventoryType;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnwrapHelper {
    public static Product unwrapProduct(ProductWrapper productWrapper, ApplicationContext applicationContext) {
        CatalogService catalogService = (CatalogService) applicationContext.getBean("blCatalogService");
        Product product = catalogService.createProduct(ProductType.PRODUCT);

        Sku sku = catalogService.createSku();
        sku.setId(productWrapper.getDefaultSku().getId());
        sku.setName(productWrapper.getDefaultSku().getName());
        sku.setActiveEndDate(productWrapper.getDefaultSku().getActiveEndDate());
        sku.setActiveStartDate(productWrapper.getDefaultSku().getActiveStartDate());
        sku.setInventoryType(InventoryType.getInstance(productWrapper.getDefaultSku().getInventoryType()));
        sku.setRetailPrice(productWrapper.getDefaultSku().getRetailPrice());
        sku.setSalePrice(productWrapper.getDefaultSku().getSalePrice());
        product.setDefaultSku(sku);

        product.setName(productWrapper.getName());
        product.setUrl(productWrapper.getUrl());
        product.setActiveStartDate(productWrapper.getActiveStartDate());
        product.setActiveEndDate(productWrapper.getActiveEndDate());
        product.setDescription(productWrapper.getDescription());
        product.setLongDescription(productWrapper.getLongDescription());
        product.setManufacturer(productWrapper.getManufacturer());
        product.setModel(productWrapper.getModel());
        product.setPromoMessage(productWrapper.getPromoMessage());
        product.setCategory(catalogService.findCategoryById(productWrapper.getDefaultCategoryId()));

        Map<String, Media> mediaMap = new HashMap<>();
        for (MediaWrapper mediaWrapper : productWrapper.getMedia()) {
            Media media = new MediaImpl();
            media.setId(mediaWrapper.getId());
            media.setTitle(mediaWrapper.getTitle());
            media.setAltText(mediaWrapper.getAltText());
            media.setTags(mediaWrapper.getTags());
            media.setUrl(mediaWrapper.getUrl());

            mediaMap.put(media.getAltText(), media);
        }
        product.setMedia(mediaMap);

        Map<String, ProductAttribute> attributeMap = new HashMap<>();
        for (ProductAttributeWrapper attributeWrapper : productWrapper.getProductAttributes()) {
            ProductAttribute attribute = new ProductAttributeImpl();
            attribute.setId(attributeWrapper.getId());
            attribute.setProduct(product);
            attribute.setName(attributeWrapper.getAttributeName());
            attribute.setValue(attributeWrapper.getAttributeValue());

            attributeMap.put(attribute.getName(), attribute);
        }
        product.setProductAttributes(attributeMap);

        return product;
    }

    public static Category unwrapCategory(CategoryWrapper categoryWrapper, ApplicationContext applicationContext) {
        CatalogService catalogService = (CatalogService) applicationContext.getBean("blCatalogService");

        Category category = catalogService.createCategory();

        category.setName(categoryWrapper.getName());
        category.setDescription(categoryWrapper.getDescription());
        category.setUrl(categoryWrapper.getUrl());
        category.setUrlKey(categoryWrapper.getUrlKey());
        category.setActiveEndDate(categoryWrapper.getActiveEndDate());
        category.setActiveStartDate(categoryWrapper.getActiveStartDate());

        List<CategoryXref> childCategoryXrefs = new ArrayList<>();
        for (CategoryWrapper subCategoryWrapper : categoryWrapper.getSubcategories()) {
            CategoryXref categoryXref = new CategoryXrefImpl();
            categoryXref.setCategory(UnwrapHelper.unwrapCategory(subCategoryWrapper, applicationContext));
            childCategoryXrefs.add(categoryXref);
        }
        category.setChildCategoryXrefs(childCategoryXrefs);

        Map<String, CategoryAttribute> attributeMap = new HashMap<>();
        for (CategoryAttributeWrapper attributeWrapper : categoryWrapper.getCategoryAttributes()) {
            CategoryAttribute attribute = new CategoryAttributeImpl();
            attribute.setId(attributeWrapper.getId());
            attribute.setCategory(category);
            attribute.setName(attributeWrapper.getAttributeName());
            attribute.setValue(attributeWrapper.getAttributeValue());

            attributeMap.put(attribute.getName(), attribute);
        }
        category.setCategoryAttributesMap(attributeMap);

        return category;
    }
}
