package com.salefinding.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jeff Fischer
 */
@Configuration
@ComponentScan("com.salefinding.core")
public class CoreConfig {

}
